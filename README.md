# simple_zabbix

This is example configure simple zabbix server with zabbix-cli via Ansible.


How use (hosts sould be exist):

```
$ git clone https://gitlab.com/screamager/simple_zabbix.git
$ cd simple_zabbix
$ ./script.sh  # Login and password will be ask.
```

If all goes well, then will configured hosts:

sz-ds01:

IP - 192.168.3.10. Zabbix server and zabbix-cli


sz-dh01:

IP - 192.168.3.11. Linux server with zabbix-agent.


Testing:

```
firefox http://192.168.3.10/zabbix/
```

Into file simple-zabbix/files/sz-ds01/tmp/zabbix-cli-input-file.txt

there're example commands adding hosts and base configuring monitor it.

Into file vars.sh defined passwords for data-base and zabbix Admin
