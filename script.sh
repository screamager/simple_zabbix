#!/usr/bin/env bash

LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

# Get login and password for connect and for sudo, but it's work if sudo asks password of user
echo -n "Enter a user login: "
read LOGIN
echo
echo -n "Enter the ssh password: "
read -s PASSWD
echo

# Vars for ansible
ANSIBLE_CONFIG="$LOCAL_DIR/ansible.cfg"
PLAYBOOKS_DIR="$LOCAL_DIR/playbooks"
CMD_HEAD="sshpass -p $PASSWD "
CMD_TAIL=" -i $LOCAL_DIR/inventory.yml --ask-pass \
-e {\"ansible_user\":\"$LOGIN\",\"ansible_become_pass\":\"$PASSWD\",\"psql_pass\":\"$ZABBIX_POSTGRESQL_PASSWORD\",\"zadm_pass\":\"$ZABBIX_GUI_ADMIN_PASSWORD\"}"

# Install and configurate zabbix server
$CMD_HEAD ansible-playbook $PLAYBOOKS_DIR/sz-ds01/playbook_install_zabbix.yml           $CMD_TAIL
$CMD_HEAD ansible-playbook $PLAYBOOKS_DIR/sz-ds01/playbook_install_zabbix-cli.yml       $CMD_TAIL
$CMD_HEAD ansible-playbook $PLAYBOOKS_DIR/sz-ds01/playbook_use_zabbix-cli.yml           $CMD_TAIL

# Install and configurate zabbix agent
$CMD_HEAD ansible-playbook $PLAYBOOKS_DIR/sz-dh01/playbook_install_zabbix-agent.yml     $CMD_TAIL

